"use strict";
const switcher = document.querySelector(".fa-moon");
const theme = localStorage.getItem('theme');

const setDark = () => {
    document.getElementById('switcher-id').href = './css/themes/dark.css';
    localStorage.setItem('theme', "dark");
};

const setLight = () => {
    document.getElementById('switcher-id').href = './css/themes/light.css';
    localStorage.setItem('theme', "light");
};

localStorage.getItem("theme") === "dark" ? setDark() : setLight();

let flag = true;
switcher.addEventListener("click", () => {
    flag ? setDark() : setLight();
    flag = !flag
});