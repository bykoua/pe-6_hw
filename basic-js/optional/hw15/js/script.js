"use strict";
const factorial = (x) => x ? x * factorial(--x) : 1;

for (let i = 1; i <= 6; i++) {
    console.log(factorial(i));
}