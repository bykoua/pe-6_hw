### Теоретический вопрос
1. Опишите своими словами разницу между функциями `setTimeout()` и `setInterval()`.
2. Что произойдет, если в функцию `setTimeout()` передать нулевую задержку? Сработает ли она мгновенно, и почему?
3. Почему важно не забывать вызывать функцию `clearInterval()`, когда ранее созданный цикл запуска вам уже не нужен?
### Ответ
1. `setTimeout()` - выполняется единожды, через указанное время.
`setInterval()` - выполняется циклично с заданным интервалом.
Время исчисляется в миллисекундах.
2. Функция сработает мгновенно, из-за значения по-умолчанию равному 0 мс. 
3. Чтобы заданная функция таймера не занимала ячейки памяти, что важно для оптимизации и быстродействия.

